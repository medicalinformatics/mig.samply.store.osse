--
-- Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
-- Copyright (C) since 2016 The Samply Community
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses>.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--

BEGIN TRANSACTION;
INSERT INTO statuses (data, transaction_id) values ('{"name":"open"}', 1);
INSERT INTO statuses (data, transaction_id) values ('{"name":"reported"}', 1);
INSERT INTO statuses (data, transaction_id) values ('{"name":"signed"}', 1);

INSERT INTO "statusChanges" (data, "from", "to", transaction_id) values ('{"name":"Report form"}', 1, 2, 1);
INSERT INTO "statusChanges" (data, "from", "to", transaction_id) values ('{"name":"Sign form"}', 2, 3, 1);
INSERT INTO "statusChanges" (data, "from", "to", transaction_id) values ('{"name":"Refuse form"}', 2, 1, 1);
INSERT INTO "statusChanges" (data, "from", "to", transaction_id) values ('{"name":"Break signature"}', 3, 1, 1);
COMMIT;
