--
-- Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
-- Copyright (C) since 2016 The Samply Community
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses>.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--

BEGIN TRANSACTION;

ALTER TABLE transactions DROP CONSTRAINT transactions_user_id_fkey;

INSERT INTO transactions ("timestamp", user_id) values (now(), 0);

INSERT INTO "users" (data, username, password, salt, "isActivated", "activationCode", "failCounter", transaction_id) VALUES ('{}', 'admin',
    '6bcf0beed43ffcbb06d778c5e00dab81f901c71162bb1ac06ceb51309829e7466219d7a6af315a6583668ddd7016e9bf72bbed1e6ba269d22d8e1c17c8c52acd',
    'gyQq5bsnpfE5RkeN5Z3iTknLQqt40VsL', 1, '', 0, currval('"transactions_id_seq"'));

UPDATE "transactions" SET "user_id" = currval('"users_id_seq"') WHERE user_id = 0;

INSERT INTO locations (transaction_id, name, data) VALUES (currval('"transactions_id_seq"'), 'Globale Rollen', '{}');

INSERT INTO roles (data, name, transaction_id, location_id) values ('{"roleType":"ADMINISTRATOR", "isUserRole":false}',
    'Globale Admins', currval('"transactions_id_seq"'), currval('"locations_id_seq"'));

INSERT INTO users_roles (transaction_id, user_id, role_id) VALUES (currval('"transactions_id_seq"'),
    currval('"users_id_seq"'), currval('"roles_id_seq"'));

INSERT INTO "users" (data, username, password, salt, "isActivated", "activationCode", "failCounter", transaction_id) VALUES ('{}', 'export',
    'x',
    '9rkq0p8YcJjOyhq21hx6mHBjpClOHzni', 1, '', 0, currval('"transactions_id_seq"'));

INSERT INTO "users" (data, username, password, salt, "isActivated", "activationCode", "failCounter", transaction_id) VALUES ('{}', 'dev',
    '2eae7295a422a807c9e51a7da9afe038d4e8b2de216e8f0d37261503e787abadc9d5bf551d095aa6043df24d6f34f73bdfd9d8350e3a214d761f96ddeba6323c',
    'kOik80sM7O5x1qUtBr3DAGz4IIdGV2pg', 1, '', 0, currval('"transactions_id_seq"'));

INSERT INTO roles (data, name, transaction_id, location_id) values ('{"roleType":"DEVELOPER", "isUserRole":false}',
    'Developer Role', currval('"transactions_id_seq"'), currval('"locations_id_seq"'));

INSERT INTO users_roles (transaction_id, user_id, role_id) VALUES (currval('"transactions_id_seq"'),
    currval('"users_id_seq"'), currval('"roles_id_seq"'));

ALTER TABLE transactions ADD FOREIGN KEY (user_id) REFERENCES users (id);

COMMIT;
