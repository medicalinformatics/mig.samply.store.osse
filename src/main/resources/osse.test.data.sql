--
-- Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
-- Copyright (C) since 2016 The Samply Community
--
-- This program is free software; you can redistribute it and/or modify it under
-- the terms of the GNU Affero General Public License as published by the Free
-- Software Foundation; either version 3 of the License, or (at your option) any
-- later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
-- details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program; if not, see <http://www.gnu.org/licenses>.
--
-- Additional permission under GNU GPL version 3 section 7:
--
-- If you modify this Program, or any covered work, by linking or combining it
-- with Jersey (https://jersey.java.net) (or a modified version of that
-- library), containing parts covered by the terms of the General Public
-- License, version 2.0, the licensors of this Program grant you additional
-- permission to convey the resulting work.
--

BEGIN;

    /*																														*/
    /* *X* = Patienten, Fälle, Proben, Formulare, Episoden																	*/
    /* 																														*/
    /* Für die Testdaten werden zwei lokale Administratoren angelegt, die jeweils für ihren									*/
    /* Standort (Frankfurt und Mainz) neue Benutzer anlegen können.															*/
    /* 																														*/
    /* Anschließend wird ein Benutzer "mainz_dok" angelegt, der jeweils neue *X* aus 										*/
    /* Frankfurt und Mainz sehen darf. Die Patienten aus Frankfurt sind zwar sichtbar, allerdings ohne ihr Pseudonym.		*/
    /* 																														*/
    /* Für den letzten Benutzer sind nur die *X* sehen und verändern, die aus Frankfurt kommen.								*/
    /* Er darf außerdem neue *X* anlegen, sofern sie dem Standort Frankfurt angehören. 										*/
    /*																														*/

    INSERT INTO "statuses" (data, transaction_id) VALUES ('{"name":"Offen"}', 1);
    INSERT INTO "statuses" (data, transaction_id) VALUES ('{"name":"Gemeldet"}', 1);
    INSERT INTO "statuses" (data, transaction_id) VALUES ('{"name":"Signiert"}', 1);

    INSERT INTO "statusChanges" ("from", "to", transaction_id) VALUES (currval('"statuses_id_seq"') - 2, currval('"statuses_id_seq"') - 1, 1);
    INSERT INTO "statusChanges" ("from", "to", transaction_id) VALUES (currval('"statuses_id_seq"') - 1, currval('"statuses_id_seq"') - 0, 1);

    /* Mainz Location */
    INSERT INTO "locations" (data, name, transaction_id) VALUES ('{}', 'Mainz', 1);

    /* Mainzer Admins */
    INSERT INTO "roles" (data, name, transaction_id, location_id) VALUES ('{"roleType":"LOCAL_ADMINISTRATOR", "isUserRole":false}',
                    'Mainz Admins', 1, currval('locations_id_seq'));

    /* Lokaler Admin "mainz" // "mainz" */
    INSERT INTO "users" (data, username, password, salt, "isActivated", "activationCode", "failCounter", transaction_id) VALUES ('{}', 'mainz', 'e6f337cea9df52e29a3564e5461a8a4daa8dcc052caab0bbd042692358c39b8aa27cd14020b853d74f3383d87aece4a7037cf833f3be1f38244395d3fb1dc8e9', '1Sw9IhKh9AEEtgCvayuxGNlIWpi1uyZ6', 1, '', 0, 1);
    INSERT INTO "users_roles" (user_id, role_id, transaction_id) VALUES (currval('users_id_seq'), currval('roles_id_seq'), 1);



    /* Mainz Dokumentarrolle */
    INSERT INTO "roles" (data, name, transaction_id, location_id) VALUES ('{"roleType":"CUSTOM", "isUserRole":false}',
                    'Mainz Dokumentare', 1, currval('locations_id_seq'));

    /* Leserecht für diese Mainzer Dokumentare Gruppe */
    INSERT INTO "permissions" (data, transaction_id, role_id) VALUES ('{"resources":["patient", "case", "sample", "episode", "episodeForm", "caseForm", "location"],
                    "access":"read", "blacklist":[],
                    "criteria":[{"resource":"location", "property":"name", "value":"Mainz", "type":"equal"}],
                    "custom":{}}', 1, currval('roles_id_seq'));

    /* Mainz Dokumentar */
    INSERT INTO "users" (data, username, password, salt, "isActivated", "activationCode", "failCounter", transaction_id) VALUES ('{}', 'mainz_dok', '10dcc0ee1a9b930f71822712c8d846a9e42e27a0373b7639db26280348aa4b6cd2f356ee8c28f589cbce8c98d915af5f1463e2d3853e38e5bd74ca905b6acfa7', 'C5eXNIrLUhCV48keyi56GK5E6FlOVP4c', 1, '', 0, 1);
    INSERT INTO "users_roles" (user_id, role_id, transaction_id) VALUES (currval('users_id_seq'), currval('roles_id_seq'), 1);

    INSERT INTO "roles" (data, name, transaction_id, location_id) VALUES ('{"roleType":"CUSTOM", "isUserRole":true}',
                    'user mainz', 1, currval('locations_id_seq'));
    INSERT INTO "users_roles" (user_id, role_id, transaction_id) VALUES (currval('users_id_seq'), currval('roles_id_seq'), 1);

    /* Leserecht für dieen Mainzer Dokumentar */
    INSERT INTO "permissions" (data, transaction_id, role_id) VALUES ('{"resources":["patient", "case", "sample", "episode", "episodeForm", "caseForm", "location"],
                    "access":"read", "blacklist":[{"resource":"patient", "property":"pseudonym"}],
                    "custom":{},
                    "criteria":[{"resource":"location", "property":"name", "value":"Frankfurt", "type":"equal"}]}', 1, currval('roles_id_seq'));

    /* Mainz Patienten */
    INSERT INTO "patients" (data, transaction_id) VALUES ('{"pseudonym":"AGETCS563ER", "age":25}', 1);
    INSERT INTO "patients_locations" (location_id, transaction_id, patient_id) VALUES (currval('locations_id_seq'), 1, currval('patients_id_seq'));





    /* Frankfurt Location */
    INSERT INTO "locations" (data, name, transaction_id) VALUES ('{}', 'Frankfurt', 1);
    INSERT INTO "roles" (data, name, transaction_id, location_id) VALUES ('{"roleType":"LOCAL_ADMINISTRATOR", "isUserRole":false}',
                    'Frankfurt Admins', 1, currval('locations_id_seq'));

    /* "frankfurt" // "frankfurt" */
    INSERT INTO "users" (data, username, password, salt, "isActivated", "activationCode", "failCounter", transaction_id) VALUES ('{}', 'frankfurt', 'f204966f0a73e0410dda23549f605a833ecc0696b95045ffc0e5c76ec24c5ae36f9c9c792e27b729cfc4ddacef73b84b68658b2138d0232a1d442a12afdd22b9', 'uxKG0QniGAz2WJtwtFe0k34obCB19jBb', 1, '', 0, 1);
    INSERT INTO "users_roles" (user_id, role_id, transaction_id) VALUES (currval('users_id_seq'), currval('roles_id_seq'), 1);


    /* Frankfurt Dokumentar  "frank_dok" // "frank_dok" mit eigener Nutzerrolle */
    INSERT INTO "users" (data, username, password, salt, "isActivated", "activationCode", "failCounter", transaction_id) VALUES ('{}', 'frank_dok', 'fc801873f7a9f2e826091f24a2b10257fcd7e172e9db48498453d6698c6a2714b0f55f0fec2fb794ba986a315cf8c9053b1714c82fd88ebe78cf56e1c248add7', 'eEwPDnGRKW0q2nk2DAHbo0av43HiW1O5', 1, '', 0, 1);

    INSERT INTO "roles" (data, name, transaction_id, location_id) VALUES ('{"roleType":"CUSTOM", "isUserRole":true}',
                    'user frankfurt', 1, currval('locations_id_seq'));
    INSERT INTO "users_roles" (user_id, role_id, transaction_id) VALUES (currval('users_id_seq'), currval('roles_id_seq'), 1);


    INSERT INTO "roles" (data, name, transaction_id, location_id) VALUES ('{"roleType":"CUSTOM", "isUserRole":false}',
                    'Frankfurt Dokumentare', 1, currval('locations_id_seq'));
    INSERT INTO "users_roles" (user_id, role_id, transaction_id) VALUES (currval('users_id_seq'), currval('roles_id_seq'), 1);


    /* Leserecht für diesen Frankfurter Dokumentare */
    INSERT INTO "permissions" (data, transaction_id, role_id) VALUES ('{"resources":["patient", "case", "sample", "episode", "episodeForm", "caseForm"],
                    "access":"read", "blacklist":[{"resource":"patient", "property":"pseudonym"}],
                    "criteria":[{"resource":"location", "property":"name", "value":"Frankfurt", "type":"equal"}],
                    "custom":{}}', 1, currval('roles_id_seq'));

    /* Schreibrecht für diesen Frankfurter Dokumentar */
    INSERT INTO "permissions" (data, transaction_id, role_id) VALUES ('{"resources":["patient", "case", "sample", "episode", "episodeForm", "caseForm"],
                    "access":"write",
                    "criteria":[{"resource":"location", "property":"name", "value":"Frankfurt", "type":"equal"}],
                    "custom":{}}', 1, currval('roles_id_seq'));

    /* Erzeugerecht für diesen Frankfurter Dokumentar */
    INSERT INTO "permissions" (data, transaction_id, role_id) VALUES ('{"resources":["patient", "case", "sample", "episode", "caseForm"],
                    "access":"create",
                    "criteria":[{"resource":"location", "property":"name", "value":"Frankfurt", "type":"equal"}],
                    "custom":{}}', 1, currval('roles_id_seq'));

    INSERT INTO "permissions" (data, transaction_id, role_id) VALUES ('{"resources":["caseForm"],
                    "access":"executeAction",
                    "name":"changeStatus",
                    "criteria":[{"resource":"location", "property":"name", "value":"Frankfurt", "type":"equal"},
                                {"resource":"status", "property":"id", "value":1, "type":"equal"}],
                    "custom":{}}', 1, currval('roles_id_seq'));

    INSERT INTO "permissions" (data, transaction_id, role_id) VALUES ('{"resources":["user"],
                    "access":"executeAction",
                    "name":"createPatientUser"}',
                    1, currval('roles_id_seq'));

    /* Frankfurt Patienten */
    INSERT INTO "patients" (data, transaction_id) VALUES ('{"pseudonym":"THTWEF456WE", "age":33}', 1);
    INSERT INTO "patients_locations" (location_id, transaction_id, patient_id) VALUES (currval('locations_id_seq'), 1, currval('patients_id_seq'));
    INSERT INTO "patients_locations" (location_id, transaction_id, patient_id) VALUES (currval('locations_id_seq') - 1, 1, currval('patients_id_seq'));

    INSERT INTO "cases" (data, transaction_id, patient_id, location_id) VALUES ('{"status":"open"}', 1, currval('patients_id_seq'), currval('locations_id_seq'));
    INSERT INTO "episodes" (data, name, case_id, transaction_id) VALUES ('{"date":"NOW"}', 'Just a normal visit', currval('cases_id_seq'), 1);

    INSERT INTO "cases" (data, transaction_id, patient_id, location_id) VALUES ('{"status":"closed"}', 1, currval('patients_id_seq'), currval('locations_id_seq') - 1);
    INSERT INTO "episodes" (data, name, case_id, transaction_id) VALUES ('{"date":"Last year or something... I am not a clock"}', 'Just an abnormal visit', currval('cases_id_seq'), 1);


    /* Dokumentar in Stuttgart */

    INSERT INTO "locations" (data, name, transaction_id) VALUES ('{}', 'Stuttgart', 1);

    INSERT INTO "users" (data, username, password, salt, "isActivated", "activationCode", "failCounter", transaction_id) VALUES ('{}', 'stutt_dok', '9746de59ab53170309a9673b7d14e7cc87e2468fad40ec3dd2067dfd008f984bdcd3d455a03c524c1f2b6af44f4b975b7beca3a134d15fd7336d49e03a677668', 'IPS8AXBKDtweFIldE06FkJ55dixiUUVG', 1, '', 0, 1);

    INSERT INTO "roles" (data, name, transaction_id, location_id) VALUES ('{"roleType":"CUSTOM", "isUserRole":false}',
                    'Stuttgart Dokumentare', 1, currval('locations_id_seq'));
    INSERT INTO "users_roles" (user_id, role_id, transaction_id) VALUES (currval('users_id_seq'), currval('roles_id_seq'), 1);

    INSERT INTO "roles" (data, name, transaction_id, location_id) VALUES ('{"roleType":"CUSTOM", "isUserRole":true}',
                    'user stutt_dok', 1, currval('locations_id_seq'));
    INSERT INTO "users_roles" (user_id, role_id, transaction_id) VALUES (currval('users_id_seq'), currval('roles_id_seq'), 1);

    /* Leserecht auf einen einzelnen Patienten für diesen Stuttgarter Dokumentar */
    INSERT INTO "permissions" (data, transaction_id, role_id) VALUES ((('{"resources":["patient", "case", "sample", "episode", "location"],
                    "access":"read", "blacklist":[{"resource":"patient", "property":"pseudonym"}],
                    "criteria":[{"resource":"patient", "property":"id", "value":' || currval('patients_id_seq') || ', "type":"equal"},
                                {"resource":"location", "property":"name", "value":"Frankfurt", "type":"equal"}],
                    "custom":{"idat":false}}')::json), 1, currval('roles_id_seq'));


    /* Pseudo context */
    INSERT INTO "contexts" (data, code, transaction_id) VALUES ('{}', 'Test Context', 1);
    INSERT INTO "pseudonyms" (data, value, context_id, transaction_id) VALUES ('{}', 'Test Pseudonym',
                currval('contexts_id_seq'), 1);



    INSERT INTO "users" (data, username, password, salt, "isActivated", "activationCode", "failCounter", transaction_id) VALUES ('{}', 'dev', 'fa0d1fe30d1a64dd523b2ea03ae44bd2d60dcb1d5267cc996690e708dad0581c6ddeda84ac532f191c31852db42de5128c5d73124b0ad7bb228a559e94204409', '2pF4dexiHh91HdOknT7wPTRMIeFntvUu', 1, '', 0, 1);

    INSERT INTO "roles" (data, name, transaction_id, location_id) VALUES ('{"roleType":"DEVELOPER", "isUserRole":false}',
                    'Developer Role', 1, 1);

    INSERT INTO "users_roles" (user_id, role_id, transaction_id) VALUES (currval('users_id_seq'), currval('roles_id_seq'), 1);

COMMIT;
