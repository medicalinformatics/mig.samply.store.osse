/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.StringReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import de.samply.share.model.osse.ObjectFactory;
import de.samply.share.model.osse.View;
import de.samply.store.DatabaseDefinitions;
import de.samply.store.JSONResource;
import de.samply.store.PSQLModel;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.InvalidOperationException;
import de.samply.store.osse.exception.UnknownAttributesException;
import de.samply.store.osse.query.QueryConfig;
import de.samply.store.osse.query.ResourceQueryBuilder;
import de.samply.store.query.ResourceQuery;
import de.samply.store.resources.Configuration;

/**
 * The OSSE model implementation. Most new methods are old and may be replaces,
 * but currently there is not time to do so.
 *
 */
public class OSSEModel extends PSQLModel<OSSEData> {

    /**
     * Load the osse resource xml.
     */
    static {
        try {
            File file = new File("conf/osse.resources.xml");
            if(file.exists()) {
                Configuration.parseResourceConfig(file.getAbsolutePath());
                logger.debug("Reading configuration file from config folder!");
            } else {
                InputStream resourceAsStream = OSSEModel.class.getClassLoader().getResourceAsStream("conf/osse.resources.xml");
                if(resourceAsStream != null) {
                    logger.debug("Reading configuration file from class path!");
                    Configuration.parseResourceConfig(resourceAsStream);
                } else {
                    logger.error("No configuration file for resources found! You will get a lot of errors...");
                }
            }
        } catch(JAXBException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Just an empty method that triggers the static block above.
     */
    public static void init() {
    }

    /**
     * Initializes the model with the database credentials in the specified file.
     * @param config
     * @throws DatabaseException
     */
    public OSSEModel(String config)
            throws DatabaseException {
        super(new OSSEAccessController(), config);
    }

    /**
     * Creates a patient user for the given patient. Needs an open transaction.
     * @param patient
     * @param username
     * @param password
     * @return
     * @throws DatabaseException
     */
    public Resource createPatientUser(Resource Case, String username, String password) throws DatabaseException {
        return (Resource) executeAction(OSSEOntology.Actions.CreatePatientUser, Case, username, password);
    }

    /**
     * Changes the patient users password. Needs an open transaction.
     * @param patient
     * @param username
     * @param password
     * @return
     * @throws DatabaseException
     */
    public Resource changePatientUserPassword(Resource user, String password) throws DatabaseException {
        return (Resource) executeAction(OSSEOntology.Actions.ChangePatientUserPassword, user, password);
    }

    /**
     * Disables the given user patient resource. Needs an open transaction.
     * @param user
     * @return
     * @throws DatabaseException
     */
    public Resource disablePatientUser(Resource user) throws DatabaseException {
        return (Resource) executeAction(OSSEOntology.Actions.DisablePatientUser, user);
    }

    /**
     * Enables the given user patient resource. Needs an open transaction.
     * @param user
     * @return
     * @throws DatabaseException
     */
    public Resource enablePatientUser(Resource user) throws DatabaseException {
        return (Resource) executeAction(OSSEOntology.Actions.EnablePatientUser, user);
    }

    /**
     * Executes and returns the result of thie given OSSE Query ID.
     * @param id
     * @return
     * @throws DatabaseException
     */
    public List<Resource> getQueryResult(int id, boolean ignoreViewfields) throws DatabaseException {

        try (PreparedStatement prepareStatement = connection.prepareStatement("SELECT * FROM osse_rest_request WHERE id = ?")) {
            prepareStatement.setInt(1, id);
            try (ResultSet resultSet = prepareStatement.executeQuery()) {
                if(resultSet.next()) {
                    String query = resultSet.getString("query");

                    JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);

                    Object unmarshal = context.createUnmarshaller().unmarshal(new StringReader(query));
                    Object object = null;

                    if(unmarshal instanceof JAXBElement<?>) {
                        object = ((JAXBElement<?>) unmarshal).getValue();
                    } else {
                        object = unmarshal;
                    }

                    if(object instanceof View) {
                        JSONResource osseConfig = getConfig(OSSEOntology.Configs.OSSE);
                        View view = (View) object;

                        QueryConfig queryConfig = new QueryConfig();
                        queryConfig.setIgnoreViewfield(ignoreViewfields);
                        ResourceQueryBuilder builder = new ResourceQueryBuilder(view, osseConfig, queryConfig);
                        return builder.execute(this);
                    } else {
                        logger.error("Query is not a view!");
                        return new ArrayList<Resource>();
                    }
                }
            }
            return new ArrayList<Resource>();
        } catch (SQLException | JAXBException | UnknownAttributesException | ParseException e) {
            e.printStackTrace();
            throw new DatabaseException(e);
        }
    }

    /**
     * Checks if a pseudonym exists for the given resource
     * @param resource
     * @return
     * @throws DatabaseException
     */
    public boolean resourceHasPseudonym(Resource resource)
            throws DatabaseException {
        return (getPseudonyms(resource).size() > 0);
    }

    /**
     * Returns all pseudonyms for the given resource
     * @param resource
     * @return
     * @throws DatabaseException
     */
    public List<Resource> getPseudonyms(Resource resource)
            throws DatabaseException {
        ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Pseudonym);
        query.addEqual("pseudonymDescribesResource", resource.getValue());
        return getResources(query);
    }

    /**
     * Counts the samples that belong to the specified group-ID
     * @param gid
     * @return
     * @throws DatabaseException
     */
    public Integer getGroupSampleCount(Integer gid) throws DatabaseException {
        Integer count = 0;
        try (PreparedStatement stmt = connection
                .prepareStatement("SELECT count(*) FROM cases AS c "
                        + "LEFT JOIN samples AS sam ON sam.case_id = c.id  "
                        + "WHERE ((sam.data->>'isDeleted') IS NULL "
                        + "OR (sam.data->>'isDeleted' != 'true')) AND c.group_id = ?;")) {
            stmt.setLong(1, gid);
            try (ResultSet set = stmt.executeQuery()) {
                if (set.next()) {
                    count = set.getInt("count");
                }
            }
        } catch (SQLException e) {
            throw new DatabaseException(e);
        }

        return count;
    }

    /**
     * Deletes a patient entirely
     * @param patientResource
     * @throws DatabaseException
     */
    public void wipeoutPatient(Resource resource) throws DatabaseException {
        wipeoutResource(resource);
    }

    /**
     * Deletes the given resource from the database. Currently only patients and cases
     * can be wiped out.
     * @param resource
     * @throws DatabaseException
     */
    public void wipeoutResource(Resource resource) throws DatabaseException {
        String type = resource.getType();

        if(!type.equals(OSSEVocabulary.Type.Case) && !type.equals(OSSEVocabulary.Type.Patient)) {
            throw new InvalidOperationException("Not able to wipe out resource of type "
                            + type);
        }

        checkTransaction();

        String table = DatabaseDefinitions.get(type).table;

        try (PreparedStatement st = connection.prepareStatement("DELETE FROM " + table + " WHERE id = ?")) {

            st.setInt(1, resource.getId());
            st.execute();

        } catch(SQLException sql) {
            throw new DatabaseException(sql);
        }

        resource.setId(0);
    }
}
