/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse.handlers;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import de.samply.store.*;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEAccessController;
import de.samply.store.osse.OSSECriteriaEntry;
import de.samply.store.osse.OSSEData;
import de.samply.store.osse.OSSERoleHandler;
import de.samply.store.osse.OSSERoleType;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;

/**
 * The role handler for the local administrator role type.
 *
 */
public class LocalAdministratorHandler extends OSSERoleHandler {

    /**
     * Initializes this handler for the given role, location and access controller.
     *
     * @param role the role resource
     * @param location the location resource
     * @param controller the access controller
     * @throws DatabaseException
     */
    public LocalAdministratorHandler(Resource role, Resource location,
            OSSEAccessController controller) throws DatabaseException {
        super(role, OSSERoleType.LOCAL_ADMINISTRATOR, location, controller);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configureGet(String type, AccessContext<OSSEData> context, Map<String, ResourceSelection> selections) throws DatabaseException {
        switch(type) {
        case OSSEVocabulary.Type.Registry:
        case OSSEVocabulary.Type.UserContact:
        case OSSEVocabulary.Type.User:
        case OSSEVocabulary.Type.Location:
        case OSSEVocabulary.Type.LocationContact:
        case OSSEVocabulary.Type.Role:
        case OSSEVocabulary.Type.Permission:
            return;
        }

        newInvalidClause(context.sql);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canCreateResource(Resource resource) throws DatabaseException {
        switch(resource.getType()) {
        case OSSEVocabulary.Type.User:
            // if there are no roles, abort
//			if(resource.getProperties(OSSEVocabulary.User.Roles).size() == 0) {
//				throw new InvalidOperationException("Creating user with no active roles!");
//			}

            // Check, if the user belongs to the same Location:
            for(Value role : resource.getProperties(OSSEVocabulary.User.Roles)) {
                if(role instanceof Identifiable) {
                    Identifiable roleIdent = (Identifiable) role;
                    ResourceQuery c = new ResourceQuery(OSSEVocabulary.Type.Role);
                    c.add(Criteria.Equal(OSSEVocabulary.Type.Role, OSSEVocabulary.ID, roleIdent.getId()));
                    Resource roleResource = getResourcesInternal(c).get(0);

                    if(!resourcesMeetsCriteria(roleResource,
                            new OSSECriteriaEntry(OSSEVocabulary.Type.Location, OSSEVocabulary.ID, new NumberLiteral(location.getId()), "equal"))) {
                        logger.error("The role is not from the same location as this local admins role");
                        return false;
                    }
                } else {
                    logger.error("The role is not identifiable!");
                    return false;
                }
            }

            return true;

        case OSSEVocabulary.Type.UserContact:
            // TODO: Think this through please...
            return true;

        default:
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canEditResource(Resource resource) throws DatabaseException {
        switch(resource.getType()) {
        case OSSEVocabulary.Type.User:
            // if there are no roles, abort
//			if(resource.getProperties(OSSEVocabulary.User.Roles).size() == 0) {
//				throw new InvalidOperationException("Editing user with no active roles!");
//			}

            ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Role);
            query.add(Criteria.Equal(OSSEVocabulary.Type.User, OSSEVocabulary.ID, resource.getId()));
            List<Resource> oldRoles = getResourcesInternal(query);
            HashSet<Integer> oldRolesSet = new HashSet<>();
            HashSet<Integer> newRolesSet = new HashSet<>();

            for(Resource role : oldRoles) {
                oldRolesSet.add(role.getId());
            }

            // Check, if the user belongs to the same Location:
            for(Value role : resource.getProperties(OSSEVocabulary.User.Roles)) {
                if(role instanceof Identifiable) {
                    logger.debug("Role is identifiable: " + role.getValue());

                    Identifiable roleIdent = (Identifiable) role;
                    newRolesSet.add(roleIdent.getId());

                    if(!oldRolesSet.contains(roleIdent.getId())) {
                        logger.debug("Role is new");
                        query = new ResourceQuery(OSSEVocabulary.Type.Role);
                        query.add(Criteria.Equal(OSSEVocabulary.Type.Role, OSSEVocabulary.ID, roleIdent.getId()));
                        query.add(Criteria.Equal(OSSEVocabulary.Type.Location, OSSEVocabulary.ID, location.getId()));

                        if(getResourcesInternal(query).size() == 0) {
                            logger.error("The role is not from the same location as this local admins role");
                            return false;
                        }
                    }
                } else {
                    logger.error("The role is not identifiable!");
                    return false;
                }
            }

            for(Resource r : oldRoles) {
                Identifiable loc = (Identifiable) r.getProperty(OSSEVocabulary.Role.Location);
                if(loc.getId() != location.getId() && !newRolesSet.contains(r.getId())) {
                    logger.debug("Adding old role: " + r.getValue());
                    resource.addProperty(OSSEVocabulary.User.Roles, r);
                }
            }

            return true;

        case OSSEVocabulary.Type.UserContact:
            query = new ResourceQuery(OSSEVocabulary.Type.UserContact);
            query.add(Criteria.Equal(OSSEVocabulary.Type.UserContact, OSSEVocabulary.ID, resource.getId()));
            query.add(Criteria.Equal(OSSEVocabulary.Type.Location, OSSEVocabulary.ID, location.getId()));
            List<Resource> contact = getResourcesInternal(query);
            return contact.size() == 1;

        case OSSEVocabulary.Type.Location:
            return resource.getId() == location.getId();

        case OSSEVocabulary.Type.LocationContact:
            Value property = resource.getProperty(OSSEVocabulary.LocationContact.Location);
            if(property != null && property instanceof Identifiable) {
                Identifiable ident = (Identifiable) property;
                return ident.getId() == location.getId();
            } else {
                return false;
            }

        default:
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canGetConfig(String name) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canSaveConfig(String name, JSONResource config) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canExecuteAction(String name, Object... parameters) {
        return false;
    }

}
