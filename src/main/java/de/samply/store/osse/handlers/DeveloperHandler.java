/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse.handlers;

import java.util.Map;

import de.samply.store.AccessContext;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.ResourceSelection;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEAccessController;
import de.samply.store.osse.OSSEData;
import de.samply.store.osse.OSSERoleHandler;
import de.samply.store.osse.OSSERoleType;

/**
 * The role handler for the developer role type. The developer role is allowed
 * to do <u>everything</u>. The developer role type should be used for development
 * only.
 *
 */
public class DeveloperHandler extends OSSERoleHandler {

    /**
     * Initializes this handler for the given role, location and access controller.
     *
     * @param role the role resource
     * @param location the location resource
     * @param controller the access controller
     * @throws DatabaseException
     */
    public DeveloperHandler(Resource role, Resource location, OSSEAccessController controller)
            throws DatabaseException {
        super(role, OSSERoleType.DEVELOPER, location, controller);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configureGet(String resource, AccessContext<OSSEData> context, Map<String, ResourceSelection> selections)
            throws DatabaseException {
        return;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canCreateResource(Resource resource)
            throws DatabaseException {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canEditResource(Resource resource) throws DatabaseException {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canGetConfig(String name) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canSaveConfig(String name, JSONResource config) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canExecuteAction(String name, Object... parameters) {
        return true;
    }
}
