/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse.query;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.samply.share.model.osse.Attribute;
import de.samply.share.model.osse.Case;
import de.samply.share.model.osse.Container;
import de.samply.share.model.osse.ObjectFactory;
import de.samply.share.model.osse.Patient;
import de.samply.share.model.osse.QueryResult;
import de.samply.store.AbstractResource;
import de.samply.store.DatabaseConstants;
import de.samply.store.DatabaseDefinitions;
import de.samply.store.JSONResource;
import de.samply.store.Literal;
import de.samply.store.PSQLModel;
import de.samply.store.Resource;
import de.samply.store.TimeLiteral;
import de.samply.store.TimestampLiteral;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.history.Transaction;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.sql.Clauses;
import de.samply.store.sql.SQLColumn;
import de.samply.store.sql.SQLColumnType;
import de.samply.store.sql.SQLQuery;
import de.samply.store.sql.SQLTable;
import de.samply.store.sql.SQLTimestampValue;
import de.samply.store.sql.clauses.Clause;
import de.samply.store.sql.functions.Greatest;
import de.samply.store.sql.functions.Least;
import de.samply.store.sql.functions.Max;
import de.samply.store.sql.functions.Min;

/**
 *
 */
public class QueryUtil {

    /**
     * The episode container name
     */
    private static final String EPISODE_CONTAINER_NAME = "Episode";

    /**
     * The record container name
     */
    private static final String RECORD_CONTAINER_NAME = "Record";

    /**
     * The prefix for Case containers
     */
    private static final String CASEFORM_CONTAINER_PREFIX = "CaseForm:";

    /**
     * The prefix for Episode containers
     */
    private static final String EPISODEFORM_CONTAINER_PREFIX = "EpisodeForm:";

    /**
     * The Prefix for URNs
     */
    private static final String URN_PREFIX = "urn:";

    /**
     * If the urn contains this identifier, it is regarded as a URN for a record
     */
    private static final String RECORD_URN_IDENTIFIER = ":record:";

    /**
     * The column property in array properties
     */
    private static final String ARRAY_PROPERTY = "column";


    /**
     *
     */
    private static final String EPISODE_DATE_PROPERTY = "episode-date";

    private static ObjectFactory factory = new ObjectFactory();

    /** The mdrkey changed. */
    public static final String MDRKEY_CHANGED = "last_changed";

    /** The mdrkey registered. */
    public static final String MDRKEY_REGISTERED = "registered_at";

    /** The mdrkey episode. */
    public static final String MDRKEY_EPISODE = "episode_date";

    /**
     * The timestamp property
     */
    public static final String TIMESTAMP = "timestamp";

    /**
     * The array for special MDR keys
     */
    public static final List<String> SPECIAL_KEYS = Arrays.asList(MDRKEY_CHANGED, MDRKEY_EPISODE, MDRKEY_REGISTERED);

    /**
     * The Dateformat for all exported timestamps.
     */
    private SimpleDateFormat exportDateFormat = new SimpleDateFormat("dd.MM.YYYY");

    private SimpleDateFormat exportTimeFormat = new SimpleDateFormat("HH:mm:ss");

    public QueryUtil() {
    }

    public QueryResult getResult(List<Resource> patients) throws DatabaseException {
        return getResult(patients, null, new HashMap<String, Date>(), false);
    }

    public QueryResult getResult(List<Resource> patients, PSQLModel<?> model, HashMap<String, Date> where, boolean filter) throws DatabaseException {

        QueryResult result = new QueryResult();

        /**
         * For every patient:
         * - For every case:
         * -- get all case forms and add their properties to the query result.
         * -- For all episodes:
         * --- get all episode forms and add their properties to the query result.
         */


        int id = 1;

        int formId = 1;
        int episodeId = 1;
        int cid = 1;

        for(Resource patient : patients) {
            Date lastChanged = null;

            if (filter) {
                List<Transaction> transactions = model.getTransactions(patient);
                Date registered = transactions.get(transactions.size() - 1).time;
                lastChanged = transactions.get(0).time;

                /**
                 * Filter those patients that are not registered in the required time frame.
                 */

                if(where.containsKey(MDRKEY_REGISTERED + "_leq")) {
                    Date leq = where.get(MDRKEY_REGISTERED + "_leq");
                    if(registered.after(leq)) {
                        continue;
                    }
                }

                if(where.containsKey(MDRKEY_REGISTERED + "_gt")) {
                    Date gt = where.get(MDRKEY_REGISTERED + "_gt");
                    if(registered.before(gt)) {
                        continue;
                    }
                }
            }

            /**
             * Continue to get all cases etc.
             */

            Patient p = new Patient();
            p.setId("" + (id++));

            List<Resource> caseList = null;

            if(model == null) {
                caseList = parseResourceValues(patient.getProperties(OSSEVocabulary.Patient.ReadOnly.Cases));
            } else {
                caseList = patient.getResources(OSSEVocabulary.Patient.ReadOnly.Cases, model);
            }

            for(Resource _case : caseList) {
                Case c = new Case();
                c.setId("" + (cid++));

                if(filter) {
                    Date lastChangedNew = model.getTransactions(_case).get(0).time;
                    if(lastChanged.before(lastChangedNew)) {
                        lastChanged = lastChangedNew;
                    }
                }

                List<Resource> caseFormList = null;

                if(model == null) {
                    caseFormList = parseResourceValues(_case.getProperties(OSSEVocabulary.Case.ReadOnly.CaseForms));
                } else {
                    caseFormList = _case.getResources(OSSEVocabulary.Case.ReadOnly.CaseForms, model);
                }

                for(Resource caseForm : caseFormList) {
                    Container formContainer = new Container();
                    formContainer.setDesignation(CASEFORM_CONTAINER_PREFIX + caseForm.getString(OSSEVocabulary.CaseForm.Name));
                    formContainer.setId("" + (formId++));
                    fillProperties(formContainer, caseForm);
                    c.getContainer().add(formContainer);

                    if(filter) {
                        Date lastChangedNew = model.getTransactions(caseForm).get(0).time;
                        if(lastChanged.before(lastChangedNew)) {
                            lastChanged = lastChangedNew;
                        }
                    }
                }

                List<Resource> episodeList = null;

                if(model == null) {
                    episodeList = parseResourceValues(_case.getProperties(OSSEVocabulary.Case.ReadOnly.Episodes));
                } else {
                    episodeList = _case.getResources(OSSEVocabulary.Case.ReadOnly.Episodes, model);
                }

                for(Resource episode : episodeList) {
                    Container episodeContainer = new Container();
                    episodeContainer.setDesignation(EPISODE_CONTAINER_NAME);
                    episodeContainer.setId("" + (episodeId++));

                    /**
                     * Check if there is a timestamp and set the property accordingly.
                     */
                    if(episode.getDefinedProperties().contains(TIMESTAMP)) {
                        Attribute dateAttr = new Attribute();
                        dateAttr.setMdrKey(EPISODE_DATE_PROPERTY);
                        dateAttr.setValue(factory.createValue(exportDateFormat.format(episode.getTimestamp(TIMESTAMP))));
                        episodeContainer.getAttribute().add(dateAttr);
                    }

                    if(filter) {
                        Date lastChangedNew = model.getTransactions(episode).get(0).time;
                        if(lastChanged.before(lastChangedNew)) {
                            lastChanged = lastChangedNew;
                        }

                        try {
                            Date epiDate = episode.getTimestamp(TIMESTAMP);

                            if(epiDate != null) {
                                if(where.containsKey(MDRKEY_EPISODE + "_gt")) {
                                    Date gt = where.get(MDRKEY_EPISODE + "_gt");
                                    if(epiDate.before(gt)) {
                                        continue;
                                    }
                                }

                                if(where.containsKey(MDRKEY_EPISODE + "_leq")) {
                                    Date leq = where.get(MDRKEY_EPISODE + "_leq");
                                    if(epiDate.after(leq)) {
                                        continue;
                                    }
                                }
                            }
                        } catch(Exception e) {
                        }
                    }

                    List<Resource> episodeFormList = null;

                    if(model == null) {
                        episodeFormList = parseResourceValues(episode.getProperties(OSSEVocabulary.Episode.ReadOnly.EpisodeForms));
                    } else {
                        episodeFormList = episode.getResources(OSSEVocabulary.Episode.ReadOnly.EpisodeForms, model);
                    }

                    for(Resource form : episodeFormList) {
                        Container formContainer = new Container();
                        formContainer.setDesignation(EPISODEFORM_CONTAINER_PREFIX + form.getString(OSSEVocabulary.CaseForm.Name));
                        formContainer.setId("" + (formId++));
                        fillProperties(formContainer, form);
                        episodeContainer.getContainer().add(formContainer);

                        if(filter) {
                            Date lastChangedNew = model.getTransactions(form).get(0).time;
                            if(lastChanged.before(lastChangedNew)) {
                                lastChanged = lastChangedNew;
                            }
                        }
                    }

                    c.getContainer().add(episodeContainer);
                }

                p.getCase().add(c);
            }

            /**
             * Filter if necessary. Use the lastChanged Date variable.
             */
            if(filter) {
                if(where.containsKey(MDRKEY_CHANGED + "_leq")) {
                    Date leq = where.get(MDRKEY_CHANGED + "_leq");
                    if(lastChanged.after(leq)) {
                        continue;
                    }
                }

                if(where.containsKey(MDRKEY_CHANGED + "_gt")) {
                    Date gt = where.get(MDRKEY_CHANGED + "_gt");
                    if(lastChanged.before(gt)) {
                        continue;
                    }
                }
            }

            result.getEntity().add(p);
        }
        return result;
    }

    /**
     * @param properties
     * @return
     */
    private static List<Resource> parseResourceValues(List<Value> properties) {
        List<Resource> target = new ArrayList<>();
        for(Value v : properties) {
            if(v instanceof Resource) {
                target.add((Resource) v);
            }
        }
        return target;
    }

    /**
     * Fills the container with the properties from the given resource.
     *
     * @param container the container for the XML result
     * @param resource the resource
     */
    private void fillProperties(Container container, AbstractResource resource) {
        for(String prop : filteredProperties(resource)) {
            for(Value value : resource.getProperties(prop)) {
                if(value instanceof Literal<?>) {
                    Attribute attr = new Attribute();
                    attr.setMdrKey(prop);
                    attr.setValue(factory.createValue(toString(value)));
                    container.getAttribute().add(attr);
                } else if(value instanceof JSONResource) {
                    /**
                     * Only repeatable elements are supported at this point.
                     */

                    JSONResource json = (JSONResource) value;

                    List<Value> values = json.getProperties(ARRAY_PROPERTY);

                    if(prop.contains(RECORD_URN_IDENTIFIER)) {
                        if(json.getDefinedProperties().contains(ARRAY_PROPERTY)) {
                            for(Value v : values) {
                                Container jsonContainer = new Container();
                                jsonContainer.setDesignation(RECORD_CONTAINER_NAME);
                                jsonContainer.setId(prop);

                                fillProperties(jsonContainer, (AbstractResource) v);
                                container.getContainer().add(jsonContainer);
                            }
                        } else {
                            Container jsonContainer = new Container();
                            jsonContainer.setDesignation(RECORD_CONTAINER_NAME);
                            jsonContainer.setId(prop);

                            fillProperties(jsonContainer, json);
                            container.getContainer().add(jsonContainer);
                        }
                    } else {
                        for(Value v : values) {
                            fillProperties(container, (AbstractResource) v);
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns the appropriate value.
     * @param object
     * @return
     */
    private String toString(Value value) {
        if(value instanceof TimestampLiteral) {
            return exportDateFormat.format(value.asTimestamp());
        } if(value instanceof TimeLiteral) {
            return exportTimeFormat.format(value.asTime());
        } else {
            return value.getValue();
        }
    }

    /**
     * Returns all properties that are urns.
     * @param resource the resource
     * @return
     */
    private List<String> filteredProperties(AbstractResource resource) {
        List<String> target = new ArrayList<>();
        for(String prop : resource.getDefinedProperties()) {
            if(prop.startsWith(URN_PREFIX)) {
                target.add(prop);
            }
        }
        return target;
    }

    /**
     * @param mdrKey
     * @return
     */
    public static boolean isSpecialDate(String mdrKey) {
        return SPECIAL_KEYS.contains(mdrKey);
    }

    public enum ClauseMode {GREATER, LESS};

    /**
     * Creates an SQLQuery for the special MDR key "last_changed". It uses the timestamp in the transactions.
     * @param date
     * @param mode
     * @return
     */
    public static SQLQuery createLastChangedQuery(Date date, ClauseMode mode) {
        SQLQuery query = new SQLQuery(DatabaseDefinitions.get(OSSEVocabulary.Type.Patient).table);
        SQLColumn id = query.getMainTable().getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        query.addSelection(id);

        SQLTable patientTransactions = query.addTable(DatabaseConstants.transactionsTable);
        SQLColumn patient_tid = patientTransactions.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        SQLColumn patient_tr_id = query.getMainTable().getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
        SQLColumn patient_time = patientTransactions.getColumn(DatabaseConstants.timeColumn, SQLColumnType.TIMESTAMP);
        query.addJoin(patientTransactions, patient_tid, patient_tr_id);



        SQLTable caseTable = query.getTable(DatabaseDefinitions.get(OSSEVocabulary.Type.Case).table);
        SQLColumn case_pid = caseTable.getColumn("patient_id", SQLColumnType.INTEGER);
        SQLColumn case_id = caseTable.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        query.addJoin(caseTable, case_pid, id);

        SQLColumn case_tr_id = caseTable.getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
        SQLTable caseTransactions = query.addTable(DatabaseConstants.transactionsTable);
        SQLColumn case_time = caseTransactions.getColumn(DatabaseConstants.timeColumn, SQLColumnType.TIMESTAMP);
        SQLColumn case_tid = caseTransactions.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        query.addJoin(caseTransactions, case_tr_id, case_tid);



        SQLTable caseFormTable = query.getTable(DatabaseDefinitions.get(OSSEVocabulary.Type.CaseForm).table);
        SQLColumn caseForm_cid = caseFormTable.getColumn("case_id", SQLColumnType.INTEGER);
        query.addJoin(caseFormTable, caseForm_cid, case_id);

        SQLColumn caseForm_tr_id = caseFormTable.getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
        SQLTable caseFormTransactions = query.addTable(DatabaseConstants.transactionsTable);
        SQLColumn caseForm_time = caseFormTransactions.getColumn(DatabaseConstants.timeColumn, SQLColumnType.TIMESTAMP);
        SQLColumn caseForm_tid = caseFormTransactions.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        query.addJoin(caseFormTransactions, caseForm_tr_id, caseForm_tid);



        SQLTable episodeTable = query.getTable(DatabaseDefinitions.get(OSSEVocabulary.Type.Episode).table);
        SQLColumn episode_cid = episodeTable.getColumn("case_id", SQLColumnType.INTEGER);
        SQLColumn episode_id = episodeTable.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        query.addJoin(episodeTable, episode_cid, case_id);

        SQLColumn episode_tr_id = episodeTable.getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
        SQLTable episodeTransactions = query.addTable(DatabaseConstants.transactionsTable);
        SQLColumn episode_time = episodeTransactions.getColumn(DatabaseConstants.timeColumn, SQLColumnType.TIMESTAMP);
        SQLColumn episode_tid = episodeTransactions.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        query.addJoin(episodeTransactions, episode_tr_id, episode_tid);



        SQLTable episodeFormTable = query.getTable(DatabaseDefinitions.get(OSSEVocabulary.Type.EpisodeForm).table);
        SQLColumn episodeForm_eid = episodeFormTable.getColumn("episode_id", SQLColumnType.INTEGER);
        query.addJoin(episodeFormTable, episodeForm_eid, episode_id);

        SQLColumn episodeForm_tr_id = episodeFormTable.getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
        SQLTable episodeFormTransactions = query.addTable(DatabaseConstants.transactionsTable);
        SQLColumn episodeForm_time = episodeFormTransactions.getColumn(DatabaseConstants.timeColumn, SQLColumnType.TIMESTAMP);
        SQLColumn episodeForm_tid = episodeFormTransactions.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        query.addJoin(episodeFormTransactions, episodeForm_tr_id, episodeForm_tid);


        query.addGroupBy(id);

        Clause clause = null;

        if(mode == ClauseMode.GREATER) {
            clause = Clauses.GreaterOrEqual(new Greatest(
                    new Max(case_time),
                    new Max(patient_time),
                    new Max(caseForm_time),
                    new Max(episodeForm_time),
                    new Max(episode_time)
                ), new SQLTimestampValue(new Timestamp(date.getTime())));
        } else {
            clause = Clauses.LessOrEqual(new Greatest(
                    new Max(case_time),
                    new Max(patient_time),
                    new Max(caseForm_time),
                    new Max(episodeForm_time),
                    new Max(episode_time)
                ), new SQLTimestampValue(new Timestamp(date.getTime())));
        }

        query.addHaving(clause);

        return query;
    }

    /**
     * @param minRegisteredDate
     * @param greater
     * @return
     */
    public static SQLQuery createRegisteredQuery(Date date, ClauseMode mode) {
        SQLQuery query = new SQLQuery(DatabaseDefinitions.get(OSSEVocabulary.Type.Patient).table);
        SQLColumn id = query.getMainTable().getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        query.addSelection(id);

        SQLTable patientTransactions = query.addTable(DatabaseConstants.transactionsTable);
        SQLColumn patient_tid = patientTransactions.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        SQLColumn patient_tr_id = query.getMainTable().getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
        SQLColumn patient_time = patientTransactions.getColumn(DatabaseConstants.timeColumn, SQLColumnType.TIMESTAMP);
        query.addJoin(patientTransactions, patient_tid, patient_tr_id);


        SQLTable patientHistoryTable = query.getTable(DatabaseDefinitions.get(OSSEVocabulary.Type.Patient).table + DatabaseConstants.historyPostfix);
        SQLColumn oid = patientHistoryTable.getColumn(DatabaseConstants.oldIdColumn, SQLColumnType.INTEGER);
        query.addJoin(patientHistoryTable, oid, id);

        SQLTable patientHistoryTransactions = query.addTable(DatabaseConstants.transactionsTable);
        SQLColumn patientHistory_tid = patientHistoryTransactions.getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        SQLColumn patientHistory_tr_id = patientHistoryTable.getColumn(DatabaseConstants.transactionsIdColumn, SQLColumnType.INTEGER);
        SQLColumn patientHistory_time = patientHistoryTransactions.getColumn(DatabaseConstants.timeColumn, SQLColumnType.TIMESTAMP);
        query.addJoin(patientHistoryTransactions, patientHistory_tid, patientHistory_tr_id);



        query.addGroupBy(id);

        Clause clause = null;

        if(mode == ClauseMode.GREATER) {
            clause = Clauses.GreaterOrEqual(new Least(
                    new Min(patientHistory_time),
                    new Min(patient_time)),
                    new SQLTimestampValue(new Timestamp(date.getTime())));
        } else {
            clause = Clauses.LessOrEqual(new Least(
                    new Min(patientHistory_time),
                    new Min(patient_time)),
                    new SQLTimestampValue(new Timestamp(date.getTime())));
        }

        query.addHaving(clause);

        return query;
    }

    public static SQLQuery createEpisodeQuery(Date date, ClauseMode mode) {
        SQLQuery query = new SQLQuery(DatabaseDefinitions.get(OSSEVocabulary.Type.Episode).table);
        SQLColumn id = query.getMainTable().getColumn(DatabaseConstants.idColumn, SQLColumnType.INTEGER);
        query.addSelection(id);

        SQLColumn timestamp = query.getMainTable().getColumn(DatabaseConstants.dataColumn, SQLColumnType.JSON_TIMESTAMP, "timestamp");

        Clause clause = null;

        if(mode == ClauseMode.GREATER) {
            clause = Clauses.GreaterOrEqual(timestamp, new SQLTimestampValue(new Timestamp(date.getTime())));
        } else {
            clause = Clauses.LessOrEqual(timestamp, new SQLTimestampValue(new Timestamp(date.getTime())));
        }

        query.addClause(clause);

        return query;
    }

}
