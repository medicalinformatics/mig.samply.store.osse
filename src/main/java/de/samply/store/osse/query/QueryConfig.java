/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse.query;

/**
 *
 */
public class QueryConfig {

    /**
     * If true, the resource query builder will ignore the revision of a dataelement for
     * the query part of the view.
     */
    private boolean ignoreQueryRevision = false;

    /**
     * If true, the resource query builder will ignore the revision of a datalement
     * for the viewfields part of the view.
     */
    private boolean ignoreViewfieldRevision = false;

    /**
     * If true, the resource query will ignore the view fields altogether.
     */
    private boolean ignoreViewfield = false;

    /**
     * @return the ignoreQueryRevision
     */
    public boolean isIgnoreQueryRevision() {
        return ignoreQueryRevision;
    }

    /**
     * @param ignoreQueryRevision the ignoreQueryRevision to set
     */
    public void setIgnoreQueryRevision(boolean ignoreQueryRevision) {
        this.ignoreQueryRevision = ignoreQueryRevision;
    }

    /**
     * @return the ignoreViewfieldRevision
     */
    public boolean isIgnoreViewfieldRevision() {
        return ignoreViewfieldRevision;
    }

    /**
     * @param ignoreViewfieldRevision the ignoreViewfieldRevision to set
     */
    public void setIgnoreViewfieldRevision(boolean ignoreViewfieldRevision) {
        this.ignoreViewfieldRevision = ignoreViewfieldRevision;
    }

    /**
     * @return the ignoreViewfield
     */
    public boolean isIgnoreViewfield() {
        return ignoreViewfield;
    }

    /**
     * @param ignoreViewfield the ignoreViewfield to set
     */
    public void setIgnoreViewfield(boolean ignoreViewfield) {
        this.ignoreViewfield = ignoreViewfield;
    }

}
