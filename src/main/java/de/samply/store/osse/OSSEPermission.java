/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.Value;
import de.samply.store.osse.OSSEOntology.CriteriaEntry;

/**
 * A permission object from the database. It specifies:
 *
 * - the access mode (read, write, create, executeAction)
 * - the resource types for which this permission object should be used
 * - the blacklisted properties (if any)
 * - the criterias (if any)
 * - a custom JSON object
 * - an optional name for the "executeAction" method name
 *
 *
 */
public class OSSEPermission {

    /**
     * The access type describes which actions can be executed with this
     * permission object: read, write, create resources, or the more generic
     * executeAction. The executeAction is executed in the OSSEAccessController.
     *
     */
    public enum AccessType {NONE(0, "none"), READ(1, "read"), WRITE(2, "write"), CREATE(4, "create"), EXECUTE_ACTION(8, "executeAction");

        public final int value;

        public final String name;

        private AccessType(int value, String name) {
            this.value = value;
            this.name = name;
        }

    };

    /**
     * The ID
     */
    public final int id;

    /**
     * The resource types
     */
    private final List<String> resourceTypes = new ArrayList<String>();

    /**
     * The access type
     */
    public final AccessType accessType;

    /**
     * The criterias
     */
    private final List<OSSECriteriaEntry> criterias = new ArrayList<>();

    /**
     * The blacklisted properties. Blacklisted properties will not be returned
     * from the model.
     */
    private final List<OSSEBlacklistProperty> blacklist = new ArrayList<>();

    /**
     * The method name (optional, used for executeMethod actions)
     */
    private String name;

    /**
     * The custom JSON object
     */
    private final JSONResource custom;

    /**
     * Creates a new OSSEPermission object from the specified parameters
     * @param id
     * @param resources
     * @param accessType
     * @param criterias
     * @param blacklist
     */
    public OSSEPermission(int id, String[] resources, AccessType accessType,
            OSSECriteriaEntry[] criterias, OSSEBlacklistProperty[] blacklist) {
        this.id = id;
        this.resourceTypes.addAll(Arrays.asList(resources));
        this.accessType = accessType;
        this.criterias.addAll(Arrays.asList(criterias));
        this.blacklist.addAll(Arrays.asList(blacklist));
        this.custom = new JSONResource();
    }

    /**
     * Parses the specified resource into an OSSEPermission object.
     * @param resource
     */
    public OSSEPermission(Resource resource) {
        switch(resource.getProperty(OSSEOntology.Permission.Access).getValue()) {
        case "read":
            this.accessType = AccessType.READ;
            break;

        case "write":
            this.accessType = AccessType.WRITE;
            break;

        case "create":
            this.accessType = AccessType.CREATE;
            break;

        case "executeAction":
            this.accessType = AccessType.EXECUTE_ACTION;
            break;

        default:
            this.accessType = AccessType.NONE;
            break;
        }

        for(Value v : resource.getProperties(OSSEOntology.Permission.Resources)) {
            resourceTypes.add(v.getValue());
        }

        for(Value value : resource.getProperties(OSSEOntology.Permission.Criteria)) {
            JSONResource criteriaResource = value.asJSONResource();
            OSSECriteriaEntry criteria = new OSSECriteriaEntry(
                    criteriaResource.getProperty(CriteriaEntry.Resource).getValue(),
                    criteriaResource.getProperty(CriteriaEntry.Property).getValue(),
                    criteriaResource.getProperty(CriteriaEntry.Value),
                    criteriaResource.getProperty(CriteriaEntry.Type).getValue());
            criterias.add(criteria);
        }

        for(Value value : resource.getProperties(OSSEOntology.Permission.Blacklist)) {
            JSONResource blacklistResource = value.asJSONResource();
            OSSEBlacklistProperty blacklistProperty =
                    new OSSEBlacklistProperty(
                            blacklistResource.getProperty(OSSEOntology.BlacklistProperty.Resource).getValue(),
                            blacklistResource.getProperty(OSSEOntology.BlacklistProperty.Property).getValue());

            this.blacklist.add(blacklistProperty);
        }

        if(resource.getProperty(OSSEOntology.Permission.Custom) != null) {
            this.custom = (JSONResource) resource.getProperty(OSSEOntology.Permission.Custom);
        } else {
            this.custom = new JSONResource();
        }

        this.name = resource.getString(OSSEOntology.Permission.Name);

        this.id = resource.getId();
    }

    /**
     * Creates a JSONResource from this instance.
     * @return
     */
    public JSONResource asJSONResource() {
        JSONResource json = new JSONResource();
        for(String s : resourceTypes) {
            json.addProperty(OSSEOntology.Permission.Resources, s);
        }
        json.setProperty(OSSEOntology.Permission.Access, accessType.toString());
        json.setProperty(OSSEVocabulary.ID, id);
        json.setProperty(OSSEOntology.Permission.Custom, custom);
        return json;
    }

    /**
     * Returns the unmodifiable list of resource types, for which this OSSE permission should be used.
     * @return
     */
    public List<String> getResources() {
        return Collections.unmodifiableList(this.resourceTypes);
    }

    /**
     * Returns the unmodifiable list of criteria entries.
     * @return
     */
    public List<OSSECriteriaEntry> getCriteriaEntries() {
        return Collections.unmodifiableList(this.criterias);
    }

    /**
     * Returns the unmodifiable list of blacklisted properties.
     * @return
     */
    public List<OSSEBlacklistProperty> getBlacklist() {
        return Collections.unmodifiableList(this.blacklist);
    }

    /**
     *
     * @param property
     * @return
     */
    public Value get(String property) {
        return this.custom.getProperty(property);
    }

    /**
     * Returns the name if there is one. Otherwise null.
     * @return
     */
    public String getName() {
        return name;
    }

}
