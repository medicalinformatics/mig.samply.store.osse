/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.samply.store.*;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.InvalidOperationException;
import de.samply.store.osse.OSSEData.PermissionSelection;
import de.samply.store.osse.OSSEOntology.CriteriaEntry;
import de.samply.store.osse.OSSEPermission.AccessType;
import de.samply.store.osse.handlers.*;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;

/**
 * The Access Controller handles the permissions in the OSSE
 * registry. Every request (e.g. getResources, saveOrUpdate etc.)
 * is checked here.
 *
 */
public class OSSEAccessController extends AccessController<OSSEData> {

    private static Logger logger = LogManager.getLogger(OSSEAccessController.class);

    /**
     * The active role. If no role has been selected null.
     */
    private Resource role = null;

    /**
     * The location for this resource.
     */
    private Resource location = null;

    /**
     * The role type of the current role.
     */
    private OSSERoleType roleType = null;

    /**
     * The role handler for the active role. Null if there is no active
     * role.
     */
    private OSSERoleHandler roleHandler;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canDeleteResource(Resource resource) throws DatabaseException {
        checkRole();

        return roleHandler.canDeleteResource(resource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canEditResource(Resource resource) throws DatabaseException {
        checkRole();
        logger.debug("Checking 'edit' permission for " + resource.getValue());
        return roleHandler.canEditResource(resource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canCreateResource(Resource resource) throws DatabaseException {
        checkRole();
        return roleHandler.canCreateResource(resource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void configureGet(AccessContext<OSSEData> context, String type, Map<String, ResourceSelection> selections) throws DatabaseException {
        // Without a valid role, the client is able to see all roles *available* to him

        if(type.equals(OSSEVocabulary.Type.Role) && roleHandler == null) {
            context.resourceQuery.add(
                    Criteria.Equal(OSSEVocabulary.Type.User, OSSEVocabulary.ID,
                                model.getUserId()));
            return;
        }

        if(type.equals(OSSEVocabulary.Type.Permission) && roleHandler == null) {
            context.resourceQuery.add(
                    Criteria.Equal(OSSEVocabulary.Type.User, OSSEVocabulary.ID,
                                model.getUserId()));
            return;
        }

        if(type.equals(OSSEVocabulary.Type.Status) || type.equals(OSSEVocabulary.Type.StatusChange)) {
            return;
        }

        checkRole();

        roleHandler.configureGet(type, context, selections);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dataRow(Resource resource, AccessContext<OSSEData> context,
            ResultSet set) throws SQLException {

        if(context.data == null) {
            return;
        }

        for(PermissionSelection data : context.data.getPermissionSelection()) {
            Object o = set.getObject(data.permissionColumn.alias);
            if(o != null) {
                resource.addProperty(OSSEOntology.ActivePermissions, data.permission.asJSONResource());
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void roleSelected(Resource role) throws DatabaseException {
        logger.debug("Role selected, getting active permissions...");

        this.role = role;
        this.roleType = OSSERoleType.valueOf(this.role.getProperty(OSSEOntology.Role.RoleType).getValue());

        ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Location);
        query.add(Criteria.Equal(OSSEVocabulary.Type.Location, OSSEVocabulary.ID,
                this.role.getProperty(OSSEVocabulary.Role.Location).asIdentifier().getId()));
        query.setFetchAdjacentResources(false);
        this.location = getResources(query, true).get(0);

        switch(roleType) {
        case ADMINISTRATOR:
            this.roleHandler = new AdministratorHandler(role, location, this);
            break;

        case LOCAL_ADMINISTRATOR:
            this.roleHandler = new LocalAdministratorHandler(role, location, this);
            break;

        case CUSTOM:
            this.roleHandler = new CustomHandler(role, location, this);
            break;

        case DEVELOPER:
            this.roleHandler = new DeveloperHandler(role, location, this);
            break;

        case SYSTEM:
            this.roleHandler = new SystemHandler(role, location, this);
            break;

        case PATIENT:
            this.roleHandler = new PatientHandler(role, location, this);
            break;

        default:
            throw new DatabaseException("Unhandled role type: " + this.roleType);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void filterProperties(Resource resource,
            AccessContext<OSSEData> context, String type) {

        if(roleHandler == null) {
            return;
        }

        // Check if there are any blacklists and use them
        for(Value v : resource.getProperties(OSSEOntology.ActivePermissions)) {
            JSONResource r = v.asJSONResource();
            OSSEPermission permission = roleHandler.getPermission(r.getProperty("id").asInteger());

            for(OSSEBlacklistProperty black : permission.getBlacklist()) {
                if(black.resourceType.equals(resource.getType())) {
                    logger.debug("Filtering blacklisted property " + black.property);
                    resource.removeProperties(black.property);
                }
            }
        }

    }

    /**
     * Checks, if a role has been selected. Returns if a role has been selected, throws otherwise
     * an InvalidOperationException.
     * @throws InvalidOperationException if there is no active role.
     */
    private void checkRole() throws InvalidOperationException {
        if(role == null) {
            throw new InvalidOperationException("No role selected!");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logout() {
        this.role = null;
        this.location = null;
        this.roleType = null;
        this.roleHandler = null;
    }

    /**
     * Gets the resources from the database, works without checking permissions.
     * @param query The resource query.
     * @return the result of resources
     * @throws DatabaseException
     */
    List<Resource> getResourcesInternal(ResourceQuery query) throws DatabaseException {
        return getResources(query, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canAccessHistory(Resource resource) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canGetConfig(String name) throws InvalidOperationException {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canSaveConfig(String name, JSONResource config) throws InvalidOperationException {
        checkRole();
        return roleHandler.canSaveConfig(name, config);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canExecuteAction(String name, Object... parameters) throws DatabaseException {
        checkRole();
        return roleHandler.canExecuteAction(name, parameters);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object executeAction(String actionName, Object... parameters) throws DatabaseException {
        if(OSSEOntology.Actions.ChangeStatus.equals(actionName)) {
            Resource form = (Resource) parameters[0];
            Resource status = (Resource) parameters[1];

            form.setProperty(OSSEVocabulary.CaseForm.Status, status);
            saveResourcesInternal(form);
        } else if(OSSEOntology.Actions.CreatePatientUser.equals(actionName)) {
            Resource Case = (Resource) parameters[0];
            String username = (String) parameters[1];
            String password = (String) parameters[2];

            Resource user = model.createUser(username, password);
            user.setProperty(OSSEOntology.User.UserType, OSSEUserType.PATIENT.toString());
            saveResourcesInternal(user);

            Resource role = model.createResource(OSSEVocabulary.Type.Role);
            role.setProperty(OSSEVocabulary.Role.Name, "Patient role " + username);
            role.setProperty(OSSEOntology.Role.RoleType, OSSERoleType.PATIENT.toString());
            role.setProperty(OSSEVocabulary.Role.Location, location);
            role.setProperty(OSSEVocabulary.Role.Users, user);
            role.setProperty(OSSEOntology.Role.IsUserRole, false);
            saveResourcesInternal(role);


            /**
             * Permissions to  for case forms
             */
            Resource permission = createPatientUserPermission(Case, role, OSSEVocabulary.Type.CaseForm, AccessType.CREATE);
            saveResourcesInternal(permission);

            permission = createPatientUserPermission(Case, role, OSSEVocabulary.Type.CaseForm, AccessType.READ);
            saveResourcesInternal(permission);

            permission = createPatientUserPermission(Case, role, OSSEVocabulary.Type.CaseForm, AccessType.WRITE);
            saveResourcesInternal(permission);

            /**
             * Permissions for episode forms
             */
            permission = createPatientUserPermission(Case, role, OSSEVocabulary.Type.EpisodeForm, AccessType.CREATE);
            saveResourcesInternal(permission);

            permission = createPatientUserPermission(Case, role, OSSEVocabulary.Type.EpisodeForm, AccessType.READ);
            saveResourcesInternal(permission);

            permission = createPatientUserPermission(Case, role, OSSEVocabulary.Type.EpisodeForm, AccessType.WRITE);
            saveResourcesInternal(permission);

            /**
             * Permission to read all cases for this patient and the patient itself.
             */
            permission = model.createResource(OSSEVocabulary.Type.Permission);
            permission.setProperty(OSSEVocabulary.Permission.Role, role);
            permission.addProperty(OSSEOntology.Permission.Access, OSSEPermission.AccessType.READ.name);
            permission.addProperty(OSSEOntology.Permission.Resources, OSSEVocabulary.Type.Patient);
            permission.addProperty(OSSEOntology.Permission.Resources, OSSEVocabulary.Type.Case);
            permission.addProperty(OSSEOntology.Permission.Resources, OSSEVocabulary.Type.Episode);

            JSONResource criteria = new JSONResource();
            criteria.setProperty(CriteriaEntry.Resource, OSSEVocabulary.Type.Case);
            criteria.setProperty(CriteriaEntry.Property, OSSEVocabulary.ID);
            criteria.setProperty(CriteriaEntry.Value, Case.getId());
            criteria.setProperty(CriteriaEntry.Type, CriteriaType.EQUAL.name);

            permission.setProperty(OSSEOntology.Permission.Criteria, criteria);
            saveResourcesInternal(permission);

            return user;
        } else if(OSSEOntology.Actions.ChangePatientUserPassword.equals(actionName)) {
            Resource user = (Resource) parameters[0];
            String password = (String) parameters[1];

            /**
             * Only handle already saved users.
             */
            if(user.getId() != 0) {
                model.changeUserPasswordByAdmin(user, password);
                saveResourcesInternal(user);
            }

            return user;
        } else if(OSSEOntology.Actions.DisablePatientUser.equals(actionName) ||
                  OSSEOntology.Actions.EnablePatientUser.equals(actionName)) {
            Resource user = (Resource) parameters[0];

            /**
             * Find the users with the internal function.
             */
            ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.User);
            query.addEqual(OSSEVocabulary.ID, user.getId());
            List<Resource> users = getResourcesInternal(query);

            if(users.size() == 1) {
                Resource userOriginal = users.get(0);

                if(OSSEOntology.Actions.DisablePatientUser.equals(actionName)) {
                    userOriginal.setProperty(BasicDB.User.IsActivated, 0);
                } else {
                    userOriginal.setProperty(BasicDB.User.IsActivated, 1);
                }

                saveResourcesInternal(userOriginal);
                return userOriginal;
            } else {
                throw new DatabaseException("User with ID " + user.getId() + " unknown!");
            }
        }

        return null;
    }

    private Resource createPatientUserPermission(Resource Case, Resource role, String formType, AccessType accessType) {
        Resource permission = model.createResource(OSSEVocabulary.Type.Permission);
        permission.setProperty(OSSEVocabulary.Permission.Role, role);
        permission.addProperty(OSSEOntology.Permission.Access, accessType.name);
        permission.addProperty(OSSEOntology.Permission.Resources, formType);

        JSONResource criteria = new JSONResource();
        criteria.setProperty(CriteriaEntry.Resource, OSSEVocabulary.Type.Case);
        criteria.setProperty(CriteriaEntry.Property, OSSEVocabulary.ID);
        criteria.setProperty(CriteriaEntry.Value, Case.getId());
        criteria.setProperty(CriteriaEntry.Type, CriteriaType.EQUAL.name);
        permission.setProperty(OSSEOntology.Permission.Criteria, criteria);

        criteria = new JSONResource();
        criteria.setProperty(CriteriaEntry.Resource, formType);
        criteria.setProperty(CriteriaEntry.Property, OSSEOntology.FormType.FormType);
        criteria.setProperty(CriteriaEntry.Value, OSSEOntology.FormType.PATIENTFORM);
        criteria.setProperty(CriteriaEntry.Type, CriteriaType.EQUAL.name);
        permission.addProperty(OSSEOntology.Permission.Criteria, criteria);

        return permission;
    }

    @Override
    public void prepareSaveResource(Resource resource) {
        resource.removeProperties(OSSEOntology.ActivePermissions);
    }

}
