/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse;

public class OSSEOntology {

    public static final String ActivePermissions = "activePermissions";

    public static class Configs {
        public static final String OSSE = "osse";
    }

    public static class Permission {
        public static final String Name = "name";
        public static final String Blacklist = "blacklist";
        public static final String Resources = "resources";
        public static final String Access = "access";
        public static final String Criteria = "criteria";
        public static final String Custom = "custom";
    }

    public static class BlacklistProperty {
        public static final String Resource = "resource";
        public static final String Property = "property";
    }

    public static class CriteriaEntry {
        public static final String Resource = "resource";
        public static final String Property = "property";
        public static final String Value = "value";
        public static final String Context = "context";
        public static final String Type = "type";

        public static class ReadOnly {
        }
    }

    public static class Role {
        public static final String RoleType = "roleType";
        public static final String IsUserRole = "isUserRole";
    }

    public static class User {
        public static final String UserType = "userType";
    }

    public static class Actions {
        public static final String ChangeStatus = "changeStatus";
        public static final String CreatePatientUser = "createPatientUser";
        public static final String ChangePatientUserPassword = "changePatientUserPassword";
        public static final String DisablePatientUser = "disablePatientUser";
        public static final String EnablePatientUser = "enablePatientUser";
    }

    public static class FormType {
        public static final String FormType = "formType";

        public static final String DEFAULT = "DEFAULT";
        public static final String PATIENTFORM = "PATIENTFORM";
    }
}
