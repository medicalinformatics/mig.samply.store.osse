/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse.test.subtests;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.samply.store.Resource;
import de.samply.store.ResourceIdentifierLiteral;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.NotAuthorizedException;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSERoleType;
import de.samply.store.osse.OSSEVocabulary;

@RunWith(value = Parameterized.class)
public class LocalAdministratorTest extends AbstractStoreTest {

    protected String username;
    protected String password;

    @Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] { { "mainz", "mainz" }, { "frankfurt", "frankfurt" } };
        return Arrays.asList(data);
    }

    public LocalAdministratorTest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Before
    public void setup() throws DatabaseException {
        super.setup(username, password);
    }

    @Test
    public void testPseudonymsResult() throws DatabaseException {
        List<Resource> pseudonyms = model.getResources(OSSEVocabulary.Type.Pseudonym);
        assertTrue(pseudonyms.size() == 0);
    }

    @Test
    public void testPatientsResults() throws DatabaseException {
        List<Resource> patients = model.getResources(OSSEVocabulary.Type.Patient);
        assertTrue(patients.size() == 0);
    }

    @Test
    public void testCreateUserSuccess() throws DatabaseException {
        Resource user = model.createUser("nobody_test_user_" + random.nextInt(), "LOL " + random.nextInt());
        user.setProperty(OSSEVocabulary.User.Roles, new ResourceIdentifierLiteral(OSSEVocabulary.Type.Role, role.getId()));

        saveResources(user);
    }

    @Test
    public void testCreateUserFail() throws DatabaseException {
        Resource user = model.createUser("nobody_test_user_" + random.nextInt(), "hahahahahahahahahahahaha");
        user.setProperty(OSSEVocabulary.User.Roles, new ResourceIdentifierLiteral(OSSEVocabulary.Type.Role, 1));

        exception.expect(NotAuthorizedException.class);
        saveResources(user);
        exception = ExpectedException.none();
    }

    @Test
    public void testCreateRoleFail() throws DatabaseException {
        Resource role = model.createResource(OSSEVocabulary.Type.Role);

        role.setProperty(OSSEVocabulary.Role.Name, "Test Rolle Mainz");
        role.setProperty(OSSEOntology.Role.RoleType, OSSERoleType.ADMINISTRATOR.toString());

        exception.expect(NotAuthorizedException.class);
        saveResources(role);
        exception = ExpectedException.none();
    }

    @Test
    public void testCreatePermissionFail() throws DatabaseException {
        Resource role = model.createResource(OSSEVocabulary.Type.Permission);

        role.setProperty(OSSEVocabulary.Permission.Role, new ResourceIdentifierLiteral(OSSEVocabulary.Type.Role, 5));

        exception.expect(NotAuthorizedException.class);
        saveResources(role);
        exception = ExpectedException.none();
    }

    @Test
    public void testCreateLocationFail() throws DatabaseException {
        Resource role = model.createResource(OSSEVocabulary.Type.Location);

        role.setProperty(OSSEVocabulary.Location.Name, "Test Location Mainz");
        role.setProperty(OSSEOntology.Role.RoleType, OSSERoleType.ADMINISTRATOR.toString());

        exception.expect(NotAuthorizedException.class);
        saveResources(role);
        exception = ExpectedException.none();
    }

    @Test
    public void testCreatePatientFail() throws DatabaseException {
        Resource patient = model.createResource(OSSEVocabulary.Type.Patient);
        patient.setProperty("age", 25);

        exception.expect(NotAuthorizedException.class);
        saveResources(patient);
        exception = ExpectedException.none();
    }

    @Test
    public void testCreateCaseFail() throws DatabaseException {
        Resource Case = model.createResource(OSSEVocabulary.Type.Case);
        Case.setProperty("age", 25);

        exception.expect(NotAuthorizedException.class);
        saveResources(Case);
        exception = ExpectedException.none();
    }
}
