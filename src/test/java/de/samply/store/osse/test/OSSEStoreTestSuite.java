/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse.test;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;

import javax.xml.bind.JAXBException;

import org.apache.logging.log4j.core.config.Configurator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import de.samply.config.util.FileFinderUtil;
import de.samply.store.DatabaseConstants;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.InvalidOperationException;
import de.samply.store.osse.OSSEModel;
import de.samply.store.osse.test.subtests.*;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
            DeveloperTest.class,
            LocalAdministratorTest.class,
            AdministratorTest.class,
            FrankfurtDokGetTest.class,
            MainzDokumentarTest.class,
            FrankfurtDokCreateTest.class,
            StuttgartDokTest.class,
            PatientUserTest.class})
public class OSSEStoreTestSuite {

    private static String path;

    @BeforeClass
    public static void setUpClass() throws DatabaseException, JAXBException, SQLException, FileNotFoundException {
        DatabaseConstants.PSQLVersionCode = 1;
        Configurator.initialize("de.samply.store.osse", "conf/log4j2.xml");
        path = FileFinderUtil.findFile("tests.osse.database.xml").getAbsolutePath();

        OSSEModel model = newModel();

        InputStream adminStream = OSSEModel.class.getClassLoader().getResourceAsStream("admin.user.sql");
        InputStream testStream = OSSEModel.class.getClassLoader().getResourceAsStream("osse.test.data.sql");

        if(adminStream == null || testStream == null) {
            throw new InvalidOperationException("File osse.tables.sql does not exist!");
        }

        model.installDatabase();
        model.executeStream(new InputStreamReader(adminStream));
        model.executeStream(new InputStreamReader(testStream));
    }

    public static OSSEModel newModel() throws DatabaseException, FileNotFoundException {
        return new OSSEModel(path);
    }

    @AfterClass
    public static void tearDownClass() {
    }
}
