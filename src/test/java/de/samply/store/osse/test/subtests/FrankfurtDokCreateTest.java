/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse.test.subtests;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import de.samply.store.Resource;
import de.samply.store.ResourceIdentifierLiteral;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.NotAuthorizedException;
import de.samply.store.osse.OSSEVocabulary;

public class FrankfurtDokCreateTest extends AbstractStoreTest {

    @Before
    public void setup() throws DatabaseException {
        super.setup("frank_dok", "frank_dok");
    }

    @Test
    public void testCreatePatientFail1() throws DatabaseException {
        Resource patient = model.createResource(OSSEVocabulary.Type.Patient);

        exception.expect(NotAuthorizedException.class);
        saveResources(patient);
        exception = ExpectedException.none();
    }

    @Test
    public void testCreatePatientFail2() throws DatabaseException {
        Resource patient = model.createResource(OSSEVocabulary.Type.Patient);
        Value location = new ResourceIdentifierLiteral(OSSEVocabulary.Type.Location, 1);

        patient.setProperty(OSSEVocabulary.Patient.Locations, location);

        exception.expect(NotAuthorizedException.class);
        saveResources(patient);
        exception = ExpectedException.none();
    }

    @Test
    public void testCreatePatient() throws DatabaseException {
        Resource patient = model.createResource(OSSEVocabulary.Type.Patient);

        patient.setProperty("name", "new test patient " + random.nextInt());
        patient.setProperty(OSSEVocabulary.Patient.Locations, this.location);

        saveResources(patient);
    }

    @Test
    public void testCreateCaseFail() throws DatabaseException {
        Resource Case = model.createResource(OSSEVocabulary.Type.Case);

        Case.setProperty(OSSEVocabulary.Case.Location, new ResourceIdentifierLiteral(OSSEVocabulary.Type.Location, 1));

        exception.expect(NotAuthorizedException.class);
        saveResources(Case);
        exception = ExpectedException.none();
    }

    @Test
    public void testCreateCase() throws DatabaseException {
        Resource patient = model.createResource(OSSEVocabulary.Type.Patient);
        patient.setProperty(OSSEVocabulary.Patient.Locations, this.location);

        Resource Case = model.createResource(OSSEVocabulary.Type.Case);

        Case.setProperty(OSSEVocabulary.Case.Location, this.location);
        Case.setProperty(OSSEVocabulary.Case.Patient, patient);

        saveResources(patient, Case);
    }

    @Test
    public void testCreateVisit() throws DatabaseException {
        Resource patient = model.createResource(OSSEVocabulary.Type.Patient);
        patient.setProperty(OSSEVocabulary.Patient.Locations, this.location);

        Resource Case = model.createResource(OSSEVocabulary.Type.Case);

        Case.setProperty(OSSEVocabulary.Case.Location, new ResourceIdentifierLiteral(OSSEVocabulary.Type.Location, 1));
        Case.setProperty(OSSEVocabulary.Case.Patient, patient);

        Resource visit = model.createResource(OSSEVocabulary.Type.Episode);

        visit.setProperty(OSSEVocabulary.Episode.Case, Case);
        visit.setProperty(OSSEVocabulary.Episode.Name, "Just another visit");

        exception.expect(NotAuthorizedException.class);
        saveResources(patient, Case, visit);
        exception = ExpectedException.none();

        saveResources(patient, Case, visit);
    }

    @Test
    public void testCreateFormFail() throws DatabaseException {
        Resource patient = model.createResource(OSSEVocabulary.Type.Patient);
        patient.setProperty(OSSEVocabulary.Patient.Locations, this.location);

        Resource Case = model.createResource(OSSEVocabulary.Type.Case);

        Case.setProperty(OSSEVocabulary.Case.Location, this.location);
        Case.setProperty(OSSEVocabulary.Case.Patient, patient);

        Resource visit = model.createResource(OSSEVocabulary.Type.Episode);

        visit.setProperty(OSSEVocabulary.Episode.Case, Case);
        visit.setProperty(OSSEVocabulary.Episode.Name, "Just another visit");

        Resource form = model.createResource(OSSEVocabulary.Type.EpisodeForm);

        form.setProperty(OSSEVocabulary.EpisodeForm.Episode, visit);
        form.setProperty(OSSEVocabulary.EpisodeForm.Name, "Just another form");
//		form.setProperty(OSSEVocabulary.EpisodeForm.State, "open");
        form.setProperty(OSSEVocabulary.EpisodeForm.Version, 1);

        exception.expect(NotAuthorizedException.class);
        saveResources(patient, Case, visit, form);
        exception = ExpectedException.none();
    }

    @Test
    public void testCreateCaseForm() throws DatabaseException {
        Resource statusOpen = getStatus("Offen");
        Resource statusReported = getStatus("Gemeldet");

        Resource patient = model.createResource(OSSEVocabulary.Type.Patient);
        patient.setProperty(OSSEVocabulary.Patient.Locations, this.location);

        Resource Case = model.createResource(OSSEVocabulary.Type.Case);

        Case.setProperty(OSSEVocabulary.Case.Location, this.location);
        Case.setProperty(OSSEVocabulary.Case.Patient, patient);

        Resource form = model.createResource(OSSEVocabulary.Type.CaseForm);

        form.setProperty(OSSEVocabulary.CaseForm.Case, Case);
        form.setProperty(OSSEVocabulary.CaseForm.Name, "First open, then reported");
        form.setProperty(OSSEVocabulary.CaseForm.Version, 1);
        form.setProperty(OSSEVocabulary.CaseForm.Status, statusOpen);

        saveResources(patient, Case, form);

        model.beginTransaction();
        model.executeAction("changeStatus", form, statusReported);
        model.commit();
    }

    @Test
    public void testCreateCaseFormFail() throws DatabaseException {
        Resource statusOpen = getStatus("Offen");
        Resource statusReported = getStatus("Signiert");

        Resource patient = model.createResource(OSSEVocabulary.Type.Patient);
        patient.setProperty(OSSEVocabulary.Patient.Locations, this.location);

        Resource Case = model.createResource(OSSEVocabulary.Type.Case);

        Case.setProperty(OSSEVocabulary.Case.Location, this.location);
        Case.setProperty(OSSEVocabulary.Case.Patient, patient);

        Resource form = model.createResource(OSSEVocabulary.Type.CaseForm);

        form.setProperty(OSSEVocabulary.CaseForm.Case, Case);
        form.setProperty(OSSEVocabulary.CaseForm.Name, "first open, then signed");
        form.setProperty(OSSEVocabulary.CaseForm.Version, 1);
        form.setProperty(OSSEVocabulary.CaseForm.Status, statusOpen);

        saveResources(patient, Case, form);

        exception.expect(NotAuthorizedException.class);
        model.beginTransaction();
        model.executeAction("changeStatus", form, statusReported);
        model.commit();
        exception = ExpectedException.none();
    }
}
