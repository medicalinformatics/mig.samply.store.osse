/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse.test.subtests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.NotAuthorizedException;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSEVocabulary;

/**
 *
 */
public class PatientUserTest extends AbstractStoreTest {

    @Before
    public void setup() throws DatabaseException {
        super.setup("patientUser", "patientUser");
    }

    @Test
    public void testSelfPatient() throws DatabaseException {
        ArrayList<Resource> patients = model.getResources(OSSEVocabulary.Type.Patient);

        assertTrue(patients.size() == 1);

        ArrayList<Resource> cases = model.getResources(OSSEVocabulary.Type.Case);

        assertTrue(cases.size() == 1);

        Resource Case = cases.get(0);

        Resource caseForm = model.createResource(OSSEVocabulary.Type.CaseForm);
        caseForm.setProperty(OSSEVocabulary.CaseForm.Case, Case);

        exception.expect(NotAuthorizedException.class);
        saveResources(caseForm);
        exception = ExpectedException.none();
    }

    @Test
    public void testCaseForm() throws DatabaseException {
        ArrayList<Resource> cases = model.getResources(OSSEVocabulary.Type.Case);
        ArrayList<Resource> statuses = model.getResources(OSSEVocabulary.Type.Status);

        Resource Case = cases.get(0);

        Resource caseForm = model.createResource(OSSEVocabulary.Type.CaseForm);
        caseForm.setProperty(OSSEVocabulary.CaseForm.Case, Case);
        caseForm.setProperty(OSSEVocabulary.CaseForm.Name, "Just a random caseform name!");
        caseForm.setProperty(OSSEVocabulary.CaseForm.Version, 1);
        caseForm.setProperty(OSSEVocabulary.CaseForm.Status, statuses.get(0));
        caseForm.setProperty(OSSEOntology.FormType.FormType, OSSEOntology.FormType.PATIENTFORM);

        saveResources(caseForm);
    }

}
