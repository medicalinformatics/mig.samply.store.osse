/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse.test.subtests;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import de.samply.store.Resource;
import de.samply.store.ResourceIdentifierLiteral;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEModel;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.osse.test.OSSEStoreTestSuite;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;

public class AbstractStoreTest {

    static OSSEModel model;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    protected Random random = new Random();
    protected Resource role;
    protected ResourceIdentifierLiteral location;

    @BeforeClass
    public static void testSetup() throws DatabaseException, FileNotFoundException {
        model = OSSEStoreTestSuite.newModel();
    }

    @AfterClass
    public static void testCleanup() throws DatabaseException {
        model.close();
    }

    @After
    public void tearDown() {
        model.logout();
    }

    public void setup(String username, String password) throws DatabaseException {
        model.login(username, password);
        assertTrue(model.getUserId() != 0);

        model.getResources(OSSEVocabulary.Type.Permission);

        ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Role);
        query.add(Criteria.Equal(OSSEVocabulary.Type.Role, "isUserRole", false));

        List<Resource> roles = model.getResources(query);
        role = roles.get(0);
        location = role.getProperty(OSSEVocabulary.Role.Location).asIdentifier();

        model.selectRole(role.getId());
    }

    protected void saveResources(Resource... resources) throws DatabaseException {
        model.beginTransaction();
        for(Resource r : resources) {
            model.saveOrUpdateResource(r);
        }
        model.commit();
    }

    protected Resource getStatus(String name) throws DatabaseException {
        ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Status);
        query.add(Criteria.Equal(OSSEVocabulary.Type.Status, "name", name));
        return model.getResources(query).get(0);
    }
}
