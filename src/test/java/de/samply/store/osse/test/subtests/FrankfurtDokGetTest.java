/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.store.osse.test.subtests;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEVocabulary;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FrankfurtDokGetTest extends AbstractStoreTest {

    @Before
    public void setup() throws DatabaseException {
        super.setup("frank_dok", "frank_dok");
    }

    @Test
    public void testPatients() throws DatabaseException {
        ArrayList<Resource> patients = model.getResources(OSSEVocabulary.Type.Patient);

        assertTrue(patients.size() == 1);

        Resource patient = patients.get(0);

        ArrayList<Resource> cases = patient.getResources(OSSEVocabulary.Patient.ReadOnly.Cases, model);

        assertTrue(cases.size() == 1);

        Resource Case = cases.get(0);

        assertTrue(Case.getProperty("status").getValue().equals("open"));

        /**
         * Create the patientUser for future tests
         */
        model.beginTransaction();
        model.createPatientUser(Case, "patientUser", "patientUser");
        model.commit();
    }

    @Test
    public void testWritePatients() throws DatabaseException {
        List<Resource> patients = model.getResources(OSSEVocabulary.Type.Patient);

        Resource patient = patients.get(0);
        patient.addProperty("name", "Paufonat");

        saveResources(patient);
    }

    @Test
    public void testCases() throws DatabaseException {
        List<Resource> cases = model.getResources(OSSEVocabulary.Type.Case);

        assertTrue(cases.size() == 1);
    }

    @Test
    public void testVisits() throws DatabaseException {
        List<Resource> visits = model.getResources(OSSEVocabulary.Type.Episode);

        assertTrue(visits.size() == 1);
    }
}
