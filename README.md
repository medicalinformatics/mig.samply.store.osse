# Samply Store OSSE

Samply Store OSSE is an OSSE specific extention of Samply Store.  It implements
the permission layer according to the OSSE requirements. It uses generic
permission objects to define permissions. Those permission objects are then
used when the user accesses resources. This layer may block, filter or allow
specific operations on resources like `read`, `create`, `write`.

# Features

- see Samply Store features for basic informations
- OSSE permission layer implementation

# Build

Use maven to build the `jar` file:

```
mvn clean package
```

Use it as a dependency:

```xml
<dependency>
    <groupId>de.mig.samply</groupId>
    <artifactId>store-osse</artifactId>
    <version>${version}</version>
</dependency>
```
